(function() {
	const API_URL = 'http://localhost:8080';
	const TODO_API_URL = `${API_URL}/todos`;

	const todoText = document.getElementById('todoText');

	fetch(TODO_API_URL)
		.then(processOkResponse)
		.then(todos => todos.forEach(createNewTodo));


	document.getElementById('addTodo').addEventListener('click', (event) => {
		event.preventDefault();
		fetch(TODO_API_URL, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ text: todoText.value })
		})
			.then(processOkResponse)
			.then(createNewTodo)
			.then(() => todoText.value = '')
			.catch(console.warn);
	});


	function createNewTodo(todo) {
		const li = document.createElement('li');
		const label = document.createElement('label');
		label.textContent = 'done';
		const span = document.createElement('span');
		span.textContent = ` ${todo.text}`;

		li.appendChild(span);
		li.appendChild(label);
		label.appendChild(createNewCheckbox(todo));
		li.appendChild(createNewEditButton(todo));
		li.appendChild(createNewDelButton(todo));
		document.getElementById('todoList').appendChild(li);
	}

	function processOkResponse(response = {}) {
		if (response.ok) {
			return response.json();
		}
		throw new Error(`Status not 200 (${response.status})`);
	}

	function createNewCheckbox(todo) {
		const checkbox = document.createElement('input');
		checkbox.type = 'checkbox';
		if (`${todo.done}` == 'true') {
		   checkbox.checked = true;
		}

		checkbox.addEventListener('click',
			(event) => {
				fetch(`${TODO_API_URL}/${todo.id}`, { method: 'PUT' })
					.then(processOkResponse)
					.catch(console.warn);
			});
		return checkbox;
	}

	function createNewEditButton(todo) {
		const editButton = document.createElement('button');
		editButton.textContent = 'Edit';
		editButton.style.marginLeft = "20px";
		editButton.addEventListener('click',
			(event) => {
				event.preventDefault();

				const li = editButton.parentElement;
				if (editButton.textContent == 'Edit') {
					const input = document.createElement('input');
					input.type = 'text';
					const span = li.getElementsByTagName('span')[0];
					input.value = span.textContent;
					li.insertBefore(input, span);
					li.removeChild(span);
					editButton.textContent = 'Save';
				}

				else if (editButton.textContent == 'Save') {
					const input = li.firstElementChild;
					const span = document.createElement('span');
					span.textContent = input.value;
					li.insertBefore(span, input);
					li.removeChild(input);
					editButton.textContent = 'Edit';
					fetch(`${TODO_API_URL}/edit/${todo.id}`, { method: 'PUT' ,
					 headers: {
                     				'Accept': 'application/json',
                     				'Content-Type': 'application/json'
                     			},
                     			body: JSON.stringify({ text: span.textContent })
                     			})
						.then(processOkResponse)
						.catch(console.warn);
				}
			});
		return editButton;

	}

	function createNewDelButton(todo) {
		const delButton = document.createElement('button');
		delButton.textContent = 'Delete';
		delButton.style.marginLeft = "20px";
		delButton.addEventListener('click',
			(event) => {
				event.preventDefault();
				fetch(`${TODO_API_URL}/${todo.id}`, { method: 'DELETE' });
				delButton.parentElement.remove();
			});
		return delButton;
	}

})();
